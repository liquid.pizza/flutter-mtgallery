import 'dart:convert';

import 'package:flutter/material.dart';
import 'urls.dart' as urls;

import 'package:transparent_image/transparent_image.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'mtGallery',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'mtGallery'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final List<String> mtg_urls_list = urls.url_list;

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      // appBar: AppBar(
      //   // Here we take the value from the MyHomePage object that was created by
      //   // the App.build method, and use it to set our appbar title.
      //   title: Text(widget.title),
      // ),
      backgroundColor: Colors.black,
      body: CardGrid(),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: _incrementCounter,
      //   tooltip: 'Increment',
      //   child: Icon(Icons.add),
      // ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}



class CardGrid extends StatelessWidget {

  final List<String> mtg_urls_list = urls.url_list;

  final String backup_url = "https://cdn34.picsart.com/162839060001202.jpeg?type=webp&to=min&r=640";

  CardGrid();

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      primary: true,
      crossAxisCount: 5,
      childAspectRatio: 1,
      children: List.generate(mtg_urls_list.length, (index) {
        return CardTile(
          img_url: mtg_urls_list[index],
          tag: mtg_urls_list[index],
        );
      }),
    );
  }
}

class CardTile extends StatelessWidget {
  
  final String img_url;
  final String tag;

  CardTile({
    this.img_url,
    this.tag,
  });
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
          showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: CardCloseup(
                img_url: this.img_url,
                tag: this.tag,
              ),
              contentPadding: EdgeInsets.all(0),
              backgroundColor: Colors.transparent,
            );
          },
          
        );
      },
      child: Card(
        child: ClipRRect(
          borderRadius: BorderRadius.circular(5),
          child: FittedBox(
            alignment: Alignment.topCenter,
            fit: BoxFit.fitWidth,
            child: CardImage(
                img_url: this.img_url,
              tag: this.tag,
            ),
          ),
        ),
      ),
    );
  }
}

class CardImage extends StatelessWidget {

  final String img_url;
  final String tag;

  CardImage({
    this.img_url,
    this.tag
  });

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: this.tag,
      child: FadeInImage.memoryNetwork(
        placeholder: kTransparentImage,
        image: this.img_url,
      ),
    );
  }
}


class CardCloseup extends StatelessWidget {
  
  final String img_url;
  final String tag;

  CardCloseup({
    this.img_url,
    this.tag
  });
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {Navigator.pop(context);},
      child: FittedBox(
        fit: BoxFit.contain,
        child: Container(
          decoration: BoxDecoration(
            // border: Border.all(
            //   color: Colors.red,
            //   width: 8,
            // ),
            // borderRadius: BorderRadius.circular(12),
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: CardImage(
              img_url: this.img_url,
              tag: this.tag,
            ),
          ),
        ),
      ),
    );
  }
}
